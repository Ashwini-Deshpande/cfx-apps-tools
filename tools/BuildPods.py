import copy
import os
import argparse
import json
import paramiko
import logging
import subprocess
import traceback
import shutil
import time
import sys
import glob

logger = logging.getLogger(__name__)

has_color = False

try:
    import colored
    from colored import stylize
    has_color = True
except:
    pass


def red(msg):
    if has_color:
        return stylize(msg, colored.fg("red"))
    else:
        return msg

package_file = 'pods.txt'
pod_root = "/opt/codebase/"
class Builder:

    def __init__(self, deploy_config=None, packages=None, packages_root=None,
                    cleanup=False, 
                    index='dev'):
        self.deploy_config = deploy_config
        
        self.packages_root = packages_root if packages_root else pod_root
        self.dist_tmp = "dist_tmp/"
        self.src_dir = 'src/python/'
        self.ssh_objects = {}
        self.packages = packages if packages and len(packages) >= 0 else self.get_all_packages()
        print("Pods to build: ", self.packages)
        self.packages_not_built = self.packages_not_deployed = \
            self.packages_local_install_failed = self.packages_installed_locally = 0
        self.package_dist_mapping = {}
        self.cleanup = cleanup
        self.index = index
        self.available_indexes = deploy_config.get('available-indexes', []) if deploy_config else []
        print("Available indexes are", self.available_indexes)
        self.dist_type = 'sdist' if self.index == 'dev' else 'bdist_wheel'
    
    def get_root_dist(self, create_if_not_exists=True):
        root_dist = 'dist/'
        if not os.path.exists(root_dist) and create_if_not_exists:
            os.mkdir(root_dist)
        return root_dist
    
    def clean_package(self, pkgName):
        dist_dir = self.get_root_dist()  + str(pkgName)
        #print("Package dist directory is ", dist_dir)
        if os.path.exists(dist_dir):
            print("Removing folder ", dist_dir)
            shutil.rmtree(dist_dir)

    def build_and_deploy(self):
        print("===> Pods to be built and deployed(if specified):", len(self.packages))
        # Publish source distribution only when publishing to dev index
        
        if self.cleanup and self.packages is not None:
            for pkg in self.packages : 
                #print("cleaning pkg :" , pkg)
                self.clean_package(pkg)       
        
        self.build_wheel()

        if self.deploy_config:
            self.deploy_to_devpi()
        else:
            print('Skipping deployment as deploy config is not specified')
        print("===> Pods successfully built and deployed(if specified): ", len(self.packages))
        print("===> Pods build failed: ", self.packages_not_built)
        print("===> Pods deployment failed: ", self.packages_not_deployed)
    
    def get_all_packages(self):        
        with open(package_file, 'r') as f:
            for line in f.readlines():
                packages = [str(p).strip('\n').strip(' ') for p in line.split(',')]  
        return packages
    
    def build_wheel(self):
        for package in self.packages:
            print('====================================')
            print('Current Pod (for wheel) is', package)
            #print('====================================')
            module_parent = self.get_module_parent(package)
            #print('module parent: ', module_parent)
            try:
                self.validate_package_info(package, module_parent)
                self.clean(to_clean="local", package=package)
                # open setup config
                dist_dir = self.get_dist_folder(package, context_root=False)
                #print('dist dir-- ', dist_dir)
                result_obj = self.run_local_command([
                    "python",
                    "-m",
                    "build"
                ],
                    cwd=module_parent)
                if result_obj.returncode != 0:
                    raise Exception("Unable to create wheel file for pod {}", package)

                # Copy distribution and cleanup
                self.copy_dist_file_to_root_dist(package)
                self.package_dist_mapping[package] = self.get_dist_file(package, context_root=False)
                self.clean(to_clean="local", package=package)
                #print('=============================================')
                print('Wheel Build completed for Current pod ', package)
                #print('=============================================')
            except Exception as e:
                print('ERROR: Unable to build wheel package {} due to {}'.format(package, e))
                traceback.print_exc()
                self.packages_not_built += 1
                self.packages.remove(package)

    def get_module_parent(self, package):
        return self.packages_root + package + "/" + self.src_dir

    def validate_package_info(self, package, module_parent):
        if not os.path.exists(os.path.join(self.packages_root, package)):
            raise Exception('Package does not exist ', os.path.join(self.packages_root, package))
        if not os.path.exists(os.path.join(module_parent)):
            raise Exception('Invalid pod structure. Needs to be {}'.format(module_parent))

    def clean(self, to_clean="", **kwargs):
        if to_clean == 'local':
            self.clean_local(package=kwargs.get('package'))        

    def clean_local(self, package):
        dist_dir = self.get_dist_folder(package, context_root=True)
        #print('dist dir: ', dist_dir)
        if os.path.exists(dist_dir):
            print("Cleaning local folder", dist_dir)
            shutil.rmtree(dist_dir)
        # build_dir = self.get_build_folder(package, context_root=True)
        # print('build_dir: ', build_dir)
        # if os.path.exists(build_dir):
        #     print("Cleaning local folder", build_dir)
        #     shutil.rmtree(build_dir)
        egg_folders = [x for x in os.listdir(self.get_module_parent(package)) if x.endswith("egg-info")]
        #print('egg_folders: ', egg_folders)
        if len(egg_folders) > 0:
            shutil.rmtree(self.get_module_parent(package) + "/" + egg_folders[0])

    def get_build_folder(self, package, context_root=False):
        if not context_root:
            return "build/"
        return self.get_module_parent(package) + 'build/'

    def get_dist_folder(self, package, context_root=False):
        if not context_root:
            return "dist/"
        return self.get_module_parent(package) + 'dist/'

    def run_local_command(self, command, **kwargs):
        result_obj = subprocess.run(command, capture_output=True, **kwargs)
        #print(result_obj.stdout.decode("utf-8"))
        
        errs = result_obj.stderr.decode("utf-8")
        if errs:
            elines = errs.split('\n')
            for eline in elines:
                if "zip_safe flag not set" in eline:
                    continue
                if not eline.strip():
                    continue
                print (red(eline))

        if result_obj.returncode != 0:
            print ("------ POD BUILD ERRORS ----------- ")
            print(result_obj.stderr.decode("utf-8"))
            print ("--------------------------------------- ")
        return result_obj

    def copy_dist_file_to_root_dist(self, package):
        dist_folder = self.get_root_dist() + package
        if not os.path.exists(dist_folder):
            os.mkdir(dist_folder)
        shutil.copy(self.get_dist_file(package, context_root=True), dist_folder)

    def get_dist_file(self, pkg, context_root=False):
        folder = self.get_dist_folder(pkg, context_root=True)
        if not os.path.exists(folder):
            return None
        files = os.listdir(folder)
        if files and len(files) > 0:
            if context_root:
                return folder + files[0]
            return files[0]

    def deploy_to_devpi(self):
        print('==> Deploying to DevPi')
        if self.index and self.index in self.available_indexes:
            commands = """devpi use {}{}:{}
                    devpi login {} --password={}
                    devpi use {}/{}
                    """.format(
                self.deploy_config.get('protocol') + "://" if self.deploy_config.get('protocol') else "",
                self.deploy_config['host'],
                self.deploy_config['port'],
                self.deploy_config['user'],
                self.deploy_config['pass'],
                self.deploy_config['user'],
                self.index
            )
            for package, dist_file in self.package_dist_mapping.items():
                try:
                    print("Deploying current pod to '%s' index" % (self.index), package, "==>", dist_file)
                    curr_command = copy.deepcopy(commands)
                    curr_command += "devpi upload --formats {} {}".format(self.dist_type,
                                                                          self.get_root_dist() + package + '/' + dist_file)

                    for command in curr_command.split("\n"):
                        result_obj = self.run_local_command(command.lstrip().split(" "))
                        if result_obj.returncode != 0:
                            raise Exception(result_obj.stdout.decode('utf-8'))
                    print("------------------------------")
                except Exception as e:
                    print("ERROR: Unable to deploy pod {} due to {}".format(package, e))
                    traceback.print_exc()
                    self.packages_not_deployed += 1
                    self.packages.remove(package)
        else:
            raise Exception("Invalid index specified. Valid options for --index are dev, qa")

if __name__ == "__main__":
    start = time.time()
    parser = argparse.ArgumentParser()
    parser.add_argument("--pkg", help="Name of the pod to be build, comma separated", required=False)
    parser.add_argument("--deploy", help="config file of the server to be deployed to")
    parser.add_argument("--cleanup", help="option to cleanup dist folder if exists", action="store_true")
    parser.add_argument("--local", help="pip install on your local setup", action="store_true")
    parser.add_argument("--todir", help="pip install to specified directory", required=False, default=None)
    parser.add_argument("--index", help="index for devpi server to upload", required=False, default=None)
    parser.add_argument("--rootdir", help="Parent directory of the pod to build", required=False)
    
    args = parser.parse_args()

    packages = str(args.pkg).split(",") if args.pkg else None
    deploy_config_file = args.deploy
    packages_root = args.rootdir
    deploy_config = None
    if deploy_config_file:
        with open(deploy_config_file, 'r') as f:
            deploy_config = json.load(f)
    print('Pods root directory: ',packages_root)
    if packages_root:
        if not str(packages_root).endswith("/"):
            packages_root = packages_root + '/'
        
    builder = Builder(deploy_config=deploy_config, 
                            packages=packages, 
                            packages_root=packages_root,
                            cleanup=args.cleanup, 
                            index=args.index)
    
    builder.build_and_deploy()
    end = time.time()
    print('Completed in {} secs'.format(round(end - start, 2)))