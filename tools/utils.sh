
function RED(){
	if command -v tput >/dev/null
	then
		tput setaf 1; echo "$@"; tput sgr0
	else
		echo "$@"
	fi
}

function RESET(){
	if command -v tput >/dev/null
	then
		tput sgr0
	fi
}

function BLUE(){
	if command -v tput >/dev/null
	then
		tput setaf 4; echo "$@"; tput sgr0
	else
		echo "$@"
	fi
}

function GREEN(){
	if command -v tput >/dev/null
	then
		tput setaf 2; echo "$@"; tput sgr0
	else
		echo "$@"
	fi
}

function ERROR(){
	RED "`date` [ERROR] $@"
}

function FATAL(){
	ERROR "$@"
	ERROR "Exiting build"
	exit 1
}

function INFO(){
	echo "`date` [INFO]  $@"
}

function WARN(){
	echo "`date` [WARN]  $@"
}

function SUCCESS(){
	GREEN "`date` [INFO]  $@"
}


