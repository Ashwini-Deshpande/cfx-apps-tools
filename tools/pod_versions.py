'''
Copyright CloudFabrix Software Inc. All rights reserved.
'''

import traceback
import os
import sys
import importlib
from tabulate import tabulate
from datetime import datetime

from string import Template
import argparse
import json

now = datetime.now()

pod_root = "/opt/codebase/"
package_file = 'pods.txt'
src_dir = 'src/python/'

build_id = os.environ.get("BUILD_ID") or "1"
today_version = now.strftime('%y.%-m.%-d')+"."+build_id

def apply_template(basePath, srcFolder, targetDir, fileName, variables):
    baseabs = os.path.abspath(basePath)
    srcabs = os.path.abspath(srcFolder)
    srcdir = srcabs[len(baseabs):]

    destdir = targetDir + '/' + srcdir.lstrip("/")

    if not os.path.isdir(destdir):
        os.makedirs(destdir)

    srcfile = os.path.join(srcFolder, fileName)
    destfile = os.path.join(destdir, fileName)

    print ("Processing {}/{} ...".format(srcFolder, fileName))
    data = ""
    with open(srcfile, "r") as fd:
        data = fd.read()
    
    data = Template(data).safe_substitute(**variables)

    with open(destfile, 'w') as fd:
        fd.write(data)

def simple_copy(basePath, srcFolder, targetDir, fileName):
    baseabs = os.path.abspath(basePath)
    srcabs = os.path.abspath(srcFolder)
    srcdir = srcabs[len(baseabs):]

    destdir = targetDir + '/' + srcdir.lstrip("/")

    if not os.path.isdir(destdir):
        os.makedirs(destdir)

    srcfile = os.path.join(srcFolder, fileName)
    destfile = os.path.join(destdir, fileName)

    print ("Copying {}/{} ...".format(srcFolder, fileName))
    with open(srcfile,'rb') as f1, open(destfile,'wb') as f2:
        buf = f1.read(8192)
        if buf:
            f2.write(buf)
            

def copy_recursive(srcdir, targetdir, variables):
    for folderName, _subfolders, filenames in os.walk(srcdir):
        for f in filenames:
            print (folderName)
            if f in [ "Dockerfile"]:
                apply_template(srcdir, folderName, targetdir, f, variables)
            else:
                simple_copy(srcdir, folderName, targetdir, f)

    print ()

def get_all_packages():   
    packages = []     
    with open(package_file, 'r') as f:
        for line in f.readlines():
            packages = [str(p).strip('\n').strip(' ') for p in line.split(',')]  
    return packages

def get_module_parent(package):
    return pod_root + package + "/" + src_dir

def validate_package_info(package, module_parent):
    if not os.path.exists(os.path.join(pod_root, package)):
        raise Exception('Pod does not exist ', os.path.join(pod_root, package))
    if not os.path.exists(os.path.join(module_parent)):
        raise Exception('Invalid pod structure. Needs to be {}'.format(module_parent))

def get_pod_versions():
    if not os.path.isdir(pod_root):
        print ("Pod root directory not found.")
        sys.exit(1)
    
    packages = get_all_packages()
    mods = {}
    for package in packages:
        module_parent = get_module_parent(package)
        validate_package_info(package, module_parent)
        setup = os.path.join(module_parent, "setup.cfg")
        #print('setup.cfg path: {}'.format(setup))
        if not os.path.isfile(setup):
            continue

        from configparser import ConfigParser       
        setup_properties = ConfigParser()
        if os.path.isfile(setup):
            setup_properties.read(setup)
        
        for section in setup_properties.sections():            
            if "metadata" != section:
                continue
            name = dict(setup_properties.items(section)).get('name')
            name = name.replace("-", "_")
            version = dict(setup_properties.items(section)).get('version')
            mods.update({name + '_version':version})       
    
    return mods  

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("--rootdir", help="Parent directory of the pods", required=False)
        
        args = parser.parse_args()
        rootdir = args.rootdir
        if rootdir:
            pod_root = rootdir
        print('Pods root directory: ',rootdir)

        variables = get_pod_versions()
        print('=============================================')
        print('Pod Versions ')
        for k,v in variables.items():
            print(k + ' - ' + v)
        print('=============================================')

        copy_recursive("templates/", "target_templates/", variables)
    except Exception as e:
        print()
        print ("ERROR: Failed: {}".format(e))
        traceback.print_exc()
        sys.exit(1)

    sys.exit(0)
