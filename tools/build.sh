#!/usr/bin/bash

if [ ! -f utils.sh ]
then
	echo "ERROR: Script must be executed from cfx-library-modules repository top level folder"
	exit 1
fi

source ./utils.sh

IMAGE_VERSION=""
export REGISTRY_FILE="registry/internal-registry.sh"
export DOCKER_FILES="templates/cfx-aia-apps"
export PODS_ROOT_DIR="/opt/codebase/"
NO_PULL=0
PODS=`cat pods.txt`

function print_help(){
	echo "Options"
	echo "   -r REGISTRY_FILE		REGISTRY_FILE (default registry/internal-registry.sh)"
	echo "   -m DOCKER_FILES		DOCKERFILES root directory(default templates/cfx-aia-apps)"
    echo "   -d PODS_ROOT_DIR       Parent directory of the pods to build(default /opt/codebase/) "
	echo "   -p PODS                Name of the pods to be build, comma separated "
	echo "   -t VERSION_TAG		    Tag to be associated with containers (ex: 21.8.11-qa or 'latest-qa' or 'latest')"
	echo "   -no-pull			    If specified, not do a git pull on the repositories"
	echo
}

while test $# -gt 0; do
	case "$1" in 
		-h|--help)
			print_help
			exit 0
			;;

		-no-pull)
		    shift
			export NO_PULL=1
			;;

		-r)
			shift
			if test $# -gt 0; then
        		export REGISTRY_FILE=$1
      		else
        		echo "no registry information specified"
        		exit 1
      		fi
      		shift
      		;;
		-m)
			shift
			if test $# -gt 0; then
        		export DOCKER_FILES=$1
      		else
        		echo "no docker files root path specified"
        		exit 1
      		fi
      		shift
      		;;
		-p)
			shift
			if test $# -gt 0; then
        		export PODS=$1
      		else
        		echo "no docker files root path specified"
        		exit 1
      		fi
      		shift
      		;;
		-t)
            shift
            if test $# -gt 0; then
                export IMAGE_VERSION=$1
            else
                echo "no version tag specified"
                exit 1
            fi
            shift
            ;;
		-d)
			shift
			if test $# -gt 0; then
				export PODS_ROOT_DIR=$1       		
      		else
        		echo "no parent directory of the packages specified"
        		exit 1
      		fi			
      		shift
      		;;
		*)
			echo "Invalid Option $1"
			print_help
			exit 1
			;;
	esac
done

if [[ $PODS_ROOT_DIR != */ ]]
then
	export PODS_ROOT_DIR=$PODS_ROOT_DIR/  
fi	

if [ "$IMAGE_VERSION" = "" ]
then
	echo
	echo "-t must be specified with VERSION_TAG"
	echo
	print_help
	exit 1
fi

if [ $NO_PULL -eq 0 ]
then
	BLUE "Checking out code cfx-apps-tools"
	echo
	git pull

	BLUE "Checking out code for pods"
	echo
	IFS=', ' read -r -a array <<< "$PODS"
	for POD in "${array[@]}"
	do
		echo "${PODS_ROOT_DIR}${POD}"
		(cd ${PODS_ROOT_DIR}${POD}; git pull)
	done
		
fi

/bin/rm -rf ./dist
/bin/rm -rf ./target_templates/

if [ ! -f $REGISTRY_FILE ]
then
	echo "Registry file not found: $REGISTRY_FILE"
	exit 1
fi

if [ ! -d $DOCKER_FILES ]
then
	echo "Docker files path not found: $DOCKER_FILES"
	exit 1
fi

BLUE "Building All Pods"
python3 BuildPods.py --pkg $PODS --cleanup --index qa-rda-dev --deploy devpi.json --rootdir $PODS_ROOT_DIR

INFO "Initializing Docker Registry..."
source $REGISTRY_FILE

INFO "Collecting Pod Versions..."
python3 pod_versions.py --rootdir $PODS_ROOT_DIR

SERVICE_ARRAY=(target_templates/cfx-aia-apps/*)

for entry in "${SERVICE_ARRAY[@]}"
do
    IFS='/' read -r -a path <<< "$entry"
    
    IMAGE_NAME=${path[2]}
    
    DOCKER_FILE="$PWD/target_templates/cfx-aia-apps/$IMAGE_NAME/Dockerfile"
    
    echo "CURRENT DOCKER_FILE=$DOCKER_FILE, IMAGE_BASE=$IMAGE_BASE, IMAGE=$IMAGE_NAME"

	INFO "Pulling base image: $DOCKER_REGISTRY/$IMAGE_BASE"
	if ! docker pull $DOCKER_REGISTRY/$IMAGE_BASE
	then
		FATAL "Failed to pull base image: ${DOCKER_REGISTRY}/${IMAGE_BASE}"
		exit 1
	fi

	INFO "Building image ${IMAGE_NAME}:${IMAGE_VERSION}"
	if ! docker build -f ${DOCKER_FILE} --build-arg DOCKER_REGISTRY=$DOCKER_REGISTRY --build-arg IMAGE_BASE=$IMAGE_BASE -t ${IMAGE_NAME}:${IMAGE_VERSION} .
	then
		FATAL "Docker build failed: ${IMAGE_NAME}:${IMAGE_VERSION}"
		exit 1
	fi
    
    INFO "Applying tags"
	if ! docker image tag ${IMAGE_NAME}:${IMAGE_VERSION} ${DOCKER_REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}
	then
		FATAL "Failed to apply tag: ${DOCKER_REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}"
		exit 1
	fi    
    
	INFO "Pushing images to registry ${IMAGE_NAME}:${IMAGE_VERSION}"
	if ! docker image push ${DOCKER_REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}
	then
		FATAL "Failed to push image: ${DOCKER_REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}"
		exit 1
	fi
    
	SUCCESS "Successfully deployed images to Docker registry: ${IMAGE_NAME}:${IMAGE_VERSION}"

	BLUE
	docker images ${DOCKER_REGISTRY}/${IMAGE_NAME}:${IMAGE_VERSION}
	RESET
    
done